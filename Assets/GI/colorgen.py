#!/usr/bin/env python3
import csv
import sys
points = {"0":[255,0,0] , "1":[0,255,0],"3":[255,255,0],"2":[0,0,255],"4":[255,0,255] }

f = open('flowerAnswer.csv', 'w')
writer = csv.writer(f)


r=0
while r <= 255:
    g=0
    while g <= 255:
        b=0
        while b <= 255:
            distance = {}
            for key,values in points.items():
                distance.update({ key : (((r-values[0])**2 + (g-values[1])**2 + (b-values[2])**2) ** 0.5   )   } )
                
            min = 9999
            min_key = "5"
            if((r==0) and (g==0) and (b==255)):
                min_key ="7"
            else:
                if((r==g)and(g==b)):
                    if (r>225):
                        min_key ="6"
                    else:
                        min_key = "5"
                else :
                    for key,values in distance.items():
                        if(values < min):
                            min = values
                            min_key = key
            print("\"",r,",",g,",",b,"\",",min_key)
            data =[str(r)+","+str(g)+","+str(b),min_key]
            writer.writerow(data)
            b+=15
        g+=15
    r+=15
