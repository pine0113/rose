using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionPage : MonoBehaviour
{
    // Start is called before the first frame update

    public Text PageNum;
    public FlowerControll flowerControll;
    public int no = 0;
    public Text flowerName;
    public Text flowerHint;
    public Text flowerAbility;
    public GameObject[] Pages;
    public GameObject lastButton;
    public GameObject nextButton;
    public GameObject locklastButton;
    public GameObject locknextButton;

    void Start()
    { 

      Pages=new GameObject[8];
      Pages[0]=GameObject.Find("Page01");
      Pages[1]=GameObject.Find("Page02");
      Pages[2]=GameObject.Find("Page03");
      Pages[3]=GameObject.Find("Page04");
      Pages[4]=GameObject.Find("Page05");
      Pages[5]=GameObject.Find("Page06");
      Pages[6]=GameObject.Find("Page07");
      Pages[7]=GameObject.Find("Page08");
      
      Pages[0].SetActive(false);    
      Pages[1].SetActive(false);    
      Pages[2].SetActive(false); 
      Pages[3].SetActive(false);    
      Pages[4].SetActive(false);    
      Pages[5].SetActive(false);    
      Pages[6].SetActive(false);    
      Pages[7].SetActive(false);    
   
      no=PlayerPrefs.GetInt("CollectPage"); 
        
      Debug.Log(no);          
      updateFlowerData();
    
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }


    public void Next()
    {
      Pages[no].SetActive(false);         
      if(no < flowerControll.flowerList.Count -1)
      {
        no += 1;
      }
      updateFlowerData();
      PageNum.text = (no+1).ToString();
    }

    public void Last()
    {
      Pages[no].SetActive(false);         
      if(no >0)
      {
        no -= 1;
      }      
      updateFlowerData();
      PageNum.text = (no+1).ToString();
      
    }

    private void updateFlowerData()
    {
      Debug.Log(no);
      flowerControll.loadFlowerList();
      
      if(flowerControll.flowerList[no].isUnlock==true)
      {
        flowerName.text = "";
        Pages[no].SetActive(true);         
      }else{        
        flowerName.text = "尚未解鎖";        
      }
      checkButton();
    }

    private void checkButton()
    {
      if(no==0)
      {
        locklastButton.SetActive(false);
        lastButton.SetActive(false);
      }else
      {
        locklastButton.SetActive(true);
        lastButton.SetActive(true);
      }

      if(no==7)
      {
        nextButton.SetActive(false);
        locknextButton.SetActive(false);
      }else
      {
        nextButton.SetActive(true);
        locknextButton.SetActive(true);
      }

    }


}
