using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject mainCamera;
    public Transform target;
    public Vector3 offset;
    [Range(1, 10)]
    public float smoothFactor;

    private void FixedUpdate()
    {
       
        Follow();

    }

    public void  Follow()
    {
        Vector3 targetPosision = target.position + offset;
        Vector3 smoothPosition = Vector3.Lerp(transform.position
            ,targetPosision,smoothFactor);

        mainCamera.transform.position = smoothPosition;
    }


    public void SetPosision(Vector3 position)
    {
        mainCamera.transform.position = new Vector3(position.x,position.y,-10);
    }


}
