using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageTurnSound : MonoBehaviour
{
    // Start is called before the first frame update
    public void onClick()
    {
        AkSoundEngine.PostEvent("Turn_Page", gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
