using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderVolume : MonoBehaviour
{
    public TextMeshProUGUI Music_Text;
    public TextMeshProUGUI SFX_Text;
    public Scrollbar thisScrollbar;
    public static float musicVolume;
    public static float SFXVolume;

    public Scrollbar musicScrollbar;
    public Scrollbar SFXScrollbar;

    private float originMusicVolume;
    private float originSFXVolume;
    // Start is called before the first frame update
    void Start()
    {

        if(PlayerPrefs.HasKey("MusicVolume"))
        {
            originMusicVolume=PlayerPrefs.GetFloat("MusicVolume");
            musicScrollbar.value=originMusicVolume;
            AkSoundEngine.SetRTPCValue("MusicVolume", originMusicVolume);
        }else
        {
            musicScrollbar.value=100f;
            AkSoundEngine.SetRTPCValue("MusicVolume", 100f);
        }

        if(PlayerPrefs.HasKey("SFXVolume"))
        {   originSFXVolume=PlayerPrefs.GetFloat("SFXVolume");
            SFXScrollbar.value=originSFXVolume;            
            AkSoundEngine.SetRTPCValue("SFXVolume", originSFXVolume);
        }
        else{
             SFXScrollbar.value=100f;            
            AkSoundEngine.SetRTPCValue("SFXVolume", 100f);

        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void keepOriginVolume()
    {
        originMusicVolume = musicScrollbar.value;
        originSFXVolume = SFXScrollbar.value;
    }

    public void RestoreOriginVolume()
    {
        musicScrollbar.value=originMusicVolume;
        SFXScrollbar.value=originSFXVolume;
        AkSoundEngine.SetRTPCValue("MusicVolume", originMusicVolume);
        AkSoundEngine.SetRTPCValue("SFXVolume", originSFXVolume);
        PlayerPrefs.SetFloat("MusicVolume",originMusicVolume); 
        PlayerPrefs.SetFloat("SFXVolume",originSFXVolume); 
    }

    public void SetSpecificVolume(string whatValue)
    {
        float ScrollbarValue = thisScrollbar.value;

        if (whatValue == "Music")
        {
            musicVolume = thisScrollbar.value;
            AkSoundEngine.SetRTPCValue("MusicVolume", musicVolume);
            PlayerPrefs.SetFloat("MusicVolume",musicVolume);   
            Music_Text.text = (musicVolume*100).ToString("000");         
        }

        if (whatValue == "SFX")
        {
            SFXVolume = thisScrollbar.value;
            AkSoundEngine.SetRTPCValue("SFXVolume", SFXVolume);
            PlayerPrefs.SetFloat("SFXVolume",SFXVolume);
            SFX_Text.text = (SFXVolume*100).ToString("000");
        }
    }
}
