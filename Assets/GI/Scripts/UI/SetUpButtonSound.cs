using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetUpButtonSound : MonoBehaviour
{
    // Start is called before the first frame update
    public void onClick()
    {
        AkSoundEngine.PostEvent("Setting", gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
