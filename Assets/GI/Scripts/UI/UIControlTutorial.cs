using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIControlTutorial : MonoBehaviour
{

    public Sprite[] images;
    public string[] info;
    private int page_no;
    public GameObject lastButton;
    public GameObject nextButton;
    public GameManager gameManager;
    private Image image_current;
    public Text text_current;

    public bool isFirstTutorial;

    // Start is called before the first frame update
    void Start()
    {
      isFirstTutorial= true;
      page_no=0;   
      image_current = GameObject.Find("TurorialImage").GetComponent<Image>();
      text_current = GameObject.Find("TutorialText").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LeavePanelCheck(){
        if(isFirstTutorial)
        {
            gameManager.EnablePlayerControll();            
            isFirstTutorial=false;
        }
    }

    public void nextPage(){

        if(page_no < images.Length-1)
        {
            page_no++;
            image_current.sprite = images[page_no];
            Debug.Log(info[page_no]);
            text_current.text = info[page_no];
            
        }
        checkButton();
    }
    public void lastPage(){
        if(page_no >0)
        {
            page_no--;
            image_current.sprite = images[page_no];
            text_current.text = info[page_no];            
        }
        checkButton();
    }

    public void leave()
    {
        page_no=0;   
        image_current.sprite = images[page_no];
        text_current.text = info[page_no];       
        checkButton();
    }


    private void checkButton()
    {
      if(page_no==0)
      {
        lastButton.SetActive(false);
      }else
      {
        lastButton.SetActive(true);
      }

      if(page_no==info.Length-1)
      {
        nextButton.SetActive(false);
      }else
      {
        nextButton.SetActive(true);
      }

    }

}
