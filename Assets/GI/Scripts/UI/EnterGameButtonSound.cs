using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterGameButtonSound : MonoBehaviour
{
    // Start is called before the first frame update
    public void onClick()
    {
        AkSoundEngine.PostEvent("Start_button01", gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
