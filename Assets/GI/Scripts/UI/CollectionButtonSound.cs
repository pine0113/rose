using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionButtonSound : MonoBehaviour
{
    // Start is called before the first frame update
   public void onClick()
    {
        AkSoundEngine.PostEvent("Collect_button", gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
