using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UIControllGame : MonoBehaviour
{
    public GameManager gameManager;
    [SerializeField] TextMeshProUGUI R_Object;
    [SerializeField] TextMeshProUGUI G_Object;
    [SerializeField] TextMeshProUGUI B_Object;


    public RectTransform R_Bar;
    public RectTransform G_Bar;
    public RectTransform B_Bar;
    public GameObject skillsPanel;
    public GameObject menuButton;
    public GameObject youcantuse;
    public PlayerController playerController;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnableMenuButton()
    {
        menuButton.SetActive(enabled);
    }

    


    public void updatePlayerRGBStatus(int R, int G, int B)
    {
        R_Object.text = R.ToString();
        G_Object.text = G.ToString();
        B_Object.text = B.ToString();

        

        R_Bar.GetComponent<RectTransform>().sizeDelta = new Vector2(((float)R)/(float)255.0*(float)430.0, 50);
        G_Bar.GetComponent<RectTransform>().sizeDelta = new Vector2(((float)G)/(float)255.0*(float)430.0, 50);
        B_Bar.GetComponent<RectTransform>().sizeDelta = new Vector2(((float)B)/(float)255.0*(float)430.0, 50);
    }
    public void EnableSkillPanel()
    {   
        menuButton.SetActive(false);
        playerController.Disable();
        skillsPanel.SetActive(true);

        skillsPanel.transform.Find("Organ Interface").transform.Find("Organ_R_Btn").GetComponent<Button>().interactable = true;
        skillsPanel.transform.Find("Organ Interface").transform.Find("Organ_G_Btn").GetComponent<Button>().interactable = true;
        skillsPanel.transform.Find("Organ Interface").transform.Find("Organ_B_Btn").GetComponent<Button>().interactable = true;

        if(gameManager.triggerCost[0]>gameManager.rGBStatus.colorValue[GameManager.ColorType.red])
        {
            Image temp = skillsPanel.transform.Find("Organ Interface").transform.Find("Organ_R_Btn").GetComponent<Image>();
            temp.color = new Color(0 , 0, 0, 0.5f);   
            
        }else
        {
            Image temp = skillsPanel.transform.Find("Organ Interface").transform.Find("Organ_R_Btn").GetComponent<Image>();
            temp.color = new Color(255f , 255f, 255f, 1f);           
        }

        if(gameManager.triggerCost[1]>gameManager.rGBStatus.colorValue[GameManager.ColorType.green])
        {
            Image temp = skillsPanel.transform.Find("Organ Interface").transform.Find("Organ_G_Btn").GetComponent<Image>();
            temp.color = new Color(0 , 0, 0, 0.5f);   
        }else
        {
            Image temp = skillsPanel.transform.Find("Organ Interface").transform.Find("Organ_G_Btn").GetComponent<Image>();
            temp.color = new Color(255f , 255f, 255f, 1f);   
        }

        if(gameManager.triggerCost[2]>gameManager.rGBStatus.colorValue[GameManager.ColorType.blue])
        {
            Image temp = skillsPanel.transform.Find("Organ Interface").transform.Find("Organ_B_Btn").GetComponent<Image>();
            temp.color = new Color(0 , 0, 0, 0.5f);   
        }else
        {
            Image temp = skillsPanel.transform.Find("Organ Interface").transform.Find("Organ_B_Btn").GetComponent<Image>();
            temp.color = new Color(255f , 255f, 255f, 1f);   
        }

        
    }

    public void EnableYouCantUse()
    {
        youcantuse.SetActive(true);
    }

    public void DisableSkillPanel()
    {
        skillsPanel.SetActive(false);
        playerController.Enable();
    }
}
