using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class UIControllGeneral : MonoBehaviour
{

    public GameObject settingsPanel;
    public GameObject skillConfirmPanel;
    public GameObject collectionPanel;

    public TMP_Text nameText;
    public TMP_Text skillText;
    public TMP_Text costText;

    // Start is called before the first frame update
    void Start()
    {        
        if(collectionPanel!=null)
            {collectionPanel.SetActive(false);}
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EndGame()
    {
        Application.Quit();
    }
    public void EnableSettingPanel()
    {
       settingsPanel.SetActive(true);
       PlayerController._isEnable = false;
    }
    
    public void DisableSettingPanel()
    {
        settingsPanel.SetActive(false);
        PlayerController._isEnable = true;
    }

    public void EnableSkillConfirmPanel()
    {
       skillConfirmPanel.SetActive(true);
    }
    
    public void DisableSkillConfirmPanel()
    {
        skillConfirmPanel.SetActive(false);
        
    }

    public void ReloadSkillConfirmPanel(string color, int[] triggerCost )
    {
        
         switch(color)
        {
            case "red": 
                 nameText.text = "破牆之菇" ;
                 skillText.text = "將當前撞擊之機關，轉換成可行走之地板磚" ;
                 costText.text = "R("+triggerCost[0].ToString()+")";
            break;
            case "green": 
                 nameText.text= "遠眺之菇" ;
                 skillText.text = "可視的區域以自己為圓心擴大為兩倍，增加能看到的範圍，持續5格距離" ;
                 costText.text = "G("+triggerCost[1].ToString()+")" ;
            
            break;
            
            case "blue": 
                 nameText.text= "反轉之菇" ;
                 skillText.text = "強制改變吸收水滴顏色後，各色目前持續的加減法狀態。（例：踩格子時，應是+15變-15，應是-15變+15）" ;
                 costText.text = "B("+triggerCost[1].ToString()+")"  ;
            break;
        }

    }

    void clear_all_perf()
    {
        PlayerPrefs.DeleteAll();
    }
}
