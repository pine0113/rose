using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButtonSound : MonoBehaviour
{
    // Start is called before the first frame update
    public void onClick()
    {
        AkSoundEngine.PostEvent("Exit_button", gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
