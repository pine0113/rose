using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGame : MonoBehaviour
{
    public GameObject Pannel_Settings;
    public GameObject Panel_Tutorial;
    // Start is called before the first frame update
    void Start()
    {
        Panel_Tutorial.SetActive(true);
        Pannel_Settings.SetActive(true);
        Panel_Tutorial.SetActive(false);
        Pannel_Settings.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
