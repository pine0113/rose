using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entry : MonoBehaviour
{
    public GameObject Pannel_Settings;
    public GameObject Panel_Tutorial;
    public GameObject Panel_Collection;
    // Start is called before the first frame update
    void Start()
    {
        Panel_Tutorial.SetActive(true);
        Pannel_Settings.SetActive(true);
        Panel_Collection.SetActive(true);
        Panel_Tutorial.SetActive(false);
        Pannel_Settings.SetActive(false);
        Panel_Collection.SetActive(false);

        if(PlayerPrefs.GetInt("IsCollectionOpen")==1)
        {
            Panel_Collection.SetActive(true);
            PlayerPrefs.SetInt("IsCollectionOpen",0);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
