using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControll : MonoBehaviour
{
    // Start is called before the first frame update
    public static float musicVolume;
    public static float SFXVolume;

    void Start()
    {
        //PlayerPrefs.DeleteAll();
        //Screen.SetResolution(1920, 1080, false);
        // set to true if you want a fullscreen game
        
    }

    // Update is called once per frame
    void Update()
    {
        musicVolume = SliderVolume.musicVolume;
        SFXVolume = SliderVolume.SFXVolume; 
    }


    public void GoToEntryScene()
    {
        SceneManager.LoadScene("Entry");
        PlayerPrefs.SetInt("CollectPage",0);   
    }

    public void GoToGameScene()
    {
        SceneManager.LoadScene("MainGame");
    }

    public void GoToCollectionScene(int flower_no)
    {
        SceneManager.LoadScene("Entry");
        PlayerPrefs.SetInt("CollectPage", flower_no);   
        PlayerPrefs.SetInt("IsCollectionOpen", 1);   
        //SceneManager.LoadScene("Collection", LoadSceneMode.Additive);
    }

    public void GoToTutorialScene()
    {
        SceneManager.LoadScene("Tutorial");
    }

    

    
}
