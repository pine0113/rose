using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{
    private Animator m_Animator;
    public Transform anchor;
    public Sprite SeedFront;
    public Sprite SeedBack;
    public Sprite SeedLeft;
    public Sprite SeedRight;
    public Sprite LeafRight;
    public Sprite LeafLeft;
    public Sprite LeafUp;
    public Sprite LeafDown;
    public Transform playerPosition;

    public const float JumpPower = 0.25f;
    public const float JumpDuration = 0.2f ;

    public event Action MoveFinishedHandler;
    private SpriteRenderer _spriteRenderer;
    private SpriteRenderer leafRenderer;

    private bool _isMoving; 

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        m_Animator = this.GetComponent<Animator>();
        leafRenderer = this.gameObject.transform.Find("leaf").GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        MoveFinishedHandler += MoveFinished;
    }


    void Update()
    {
        
    }

    private void MoveFinished()
    {
        this._isMoving = false;
        m_Animator.enabled = true;
    }

    public void Move(Vector2 moveVector)
    {
        if (_isMoving) return;

        _isMoving = true;
        m_Animator.enabled = false;
        UpdateSprite(moveVector);

        var destination = new Vector3(anchor.transform.position.x + moveVector.x,anchor.transform.position.y + moveVector.y,0);
        Debug.Log("Start Walking");
        Sequence s = DOTween.Sequence();    
        s.Append( playerPosition.DOMove(destination, JumpDuration, false));     
        //s.Join( transform.DOJump(destination, JumpPower, 2, JumpDuration));
        s.OnComplete(() =>  MoveFinishedHandler?.Invoke());
        //Debug.Log("Start Play Walking");
        s.Play();        
        //Debug.Log("Walk Finished");
        
    }

    public void Blocked(Vector2 moveVector)
    {
 
        if (_isMoving) return;
        m_Animator.enabled = false;
        _isMoving = true;

        UpdateSprite(moveVector);

        Vector3 origin = transform.position;
        Vector3 destination = new Vector3(anchor.transform.position.x + moveVector.x/4,anchor.transform.position.y + moveVector.y/4,0);
        
        Sequence s = DOTween.Sequence();        
        s.Append( transform.DOJump(destination, JumpPower, 1, JumpDuration));
        s.Append( transform.DOJump(origin,JumpPower, 1, JumpDuration).SetEase(Ease.OutBounce));
        s.OnComplete(() =>  MoveFinishedHandler?.Invoke());
        s.Play();      
        Debug.Log("Blocked Finished");
       
    }

    
    public void UpdateSprite(Vector2 moveVector)
    {
        if(moveVector== new Vector2(0,1))
        {
            _spriteRenderer.sprite = SeedBack;
            leafRenderer.sprite = LeafUp;
        }
        
        if(moveVector== new Vector2(0,-1))
        {
            _spriteRenderer.sprite = SeedFront;
            leafRenderer.sprite = LeafDown;
        }

        if(moveVector== new Vector2(1,0))
        {
            _spriteRenderer.sprite = SeedRight;
            leafRenderer.sprite = LeafRight;
        }

        if(moveVector== new Vector2(-1,0))
        {
            _spriteRenderer.sprite = SeedLeft;
            leafRenderer.sprite = LeafLeft;
        }
       
    }
    

}
