using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Player _player;
    public Transform playerPosition;
    private PlayerMovementInvoker _playerMovementInvoker;
    
    //private List<MoveDirectAction> _commandHistory;

    public static bool _isEnable=true;
    private void Awake()
    {
        _playerMovementInvoker = new PlayerMovementInvoker();
        _player.MoveFinishedHandler+=PlayerMovementFinished;
        
        //_commandHistory = new List<MoveDirectAction>();
    }

    private void MovementExecute(ICommand directCommand)
    {
         //_commandHistory.Add(_directAction);
        _playerMovementInvoker.SetCommand(directCommand);
        _playerMovementInvoker?.Invoke();      
    }
        
    private void PlayerMovementFinished()
    { 
         _isEnable=true;
         return;

        throw new NotImplementedException();
    }
    
    public bool isEnable()
    {
        return _isEnable;
    }
    void Start()
    {
       
    }

    // Update is called once per frame
    private void Update()
    {   
         
       
    }

    public void Enable()
    {
        _isEnable = true;
    }
    public void Disable()
    {
        _isEnable = false;
    }

    public void SetPlayerPosition(Vector3 position)
    {
        playerPosition.position = position;
        _player.transform.position = position;                        
    }

    public Vector3 GetPlayerPosition()
    {
        return _player.transform.position;                        
    }

    public void GetHistoryCommands()
    {
        _playerMovementInvoker.GetHistoryCommands();
    }

    public void PlayerBlocked(Vector2 movement)
    {
          _player.Blocked(movement);
        AkSoundEngine.PostEvent("Charater_Step_Wall", gameObject);
    }
    public void PlayerMove(Vector2 movement)
    {
        MovementExecute(new PlayerMoveCommand(_player, new MoveDirectAction(movement)));
        AkSoundEngine.PostEvent("Charater_Step", gameObject);
    }

}



public class PlayerMovementEventArgs : EventArgs
{
    public int Threshold { get; set; }
    public DateTime TimeReached { get; set; }

}


#region Command Pattern: ICommand
public interface ICommand
{
   void ExecuteAction();
}


public class PlayerMoveCommand : ICommand
{
    Player _player;
    MoveDirectAction _directAction;
    public PlayerMoveCommand(Player player, MoveDirectAction directAction)
    {
        _player = player;
        _directAction = directAction;
        
    }
    public void ExecuteAction()
    {         
        _player.Move(_directAction.vector);
    }

    public MoveDirectAction DirectAction()
    {
        return _directAction;
    }
}

public class  MoveDirectAction
{
    public enum MoveDirect
    {
        Up,
        Down,
        Left,
        Right
    }
    public MoveDirect moveDirect{set; get;}
    public Vector2 vector{set; get;}
    public MoveDirectAction(Vector2 vector)
    {
        this.vector = vector;
        if (vector == Vector2.up)
            moveDirect = MoveDirect.Up;

        if (vector == Vector2.down)
            moveDirect = MoveDirect.Down;

        if (vector == Vector2.left)
            moveDirect = MoveDirect.Left;

        if (vector == Vector2.right)
            moveDirect = MoveDirect.Right;

    }
}

#endregion


#region Command Pattern: Invoker
public class PlayerMovementInvoker
{
    private List<ICommand> _commands;
    private ICommand _command;

    private int stepCounter;

    public PlayerMovementInvoker()
    {
        _commands = new List<ICommand>();
        stepCounter = 0;
    }

    public void SetCommand(ICommand command) => _command =command;
    public void Invoke()
    {
        stepCounter ++;
        _commands.Add(_command);
        _command.ExecuteAction();
        Debug.Log(stepCounter);
    }

    public void GetHistoryCommands()
    {
        foreach(PlayerMoveCommand c in _commands)
        {
       //     Debug.Log(c.DirectAction().moveDirect);
        };
    }

}

#endregion

