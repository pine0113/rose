using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimControll : MonoBehaviour
{
    public GameManager gameManager;
    public GameObject skipButton;
    public GameObject skipEndButton;
    public GameObject askTutorial;
    public GameObject AnimBG;
    public Animator playerAnim;
    private bool donotShowWannaTutorial;

    // Start is called before the first frame update
    void Awake()
    {
      AnimBG.SetActive(false);
    }

    void Start()
    {
  
        donotShowWannaTutorial = Convert.ToBoolean(PlayerPrefs.GetInt("DonotShowWannaTutorial"));
    }

    public void start_anim1()
    {
        AnimBG.SetActive(true);
        gameManager.animScene_start[0].SetActive(true);
        AkSoundEngine.PostEvent("IntroAni", gameObject);
    }

    public void start_anim2()
    {
        gameManager.animScene_start[0].SetActive(false);
        gameManager.animScene_start[1].SetActive(true);
   
    }

    public void start_anim2_end()
    {
        gameManager.animScene_start[1].SetActive(false);
        skipButton.SetActive(false);

        AkSoundEngine.PostEvent("IntroAniStop", gameObject); 
        gameManager.DisablePlayerControll();
        if(!donotShowWannaTutorial){
            askTutorial.SetActive(true);
        }
        else
        {
            gameManager.EnablePlayerControll();
        }
        AnimBG.SetActive(false);
        playerAnim.enabled=true;
        playerAnim.Play("Idle");
    }

    public void end_anim1()
    {
        AnimBG.SetActive(true);
        skipEndButton.SetActive(true);
        gameManager.animScene_end[0].SetActive(true);
        AkSoundEngine.PostEvent("OutroAni", gameObject);
    }

    public void end_anim2()
    {
        gameManager.animScene_end[0].SetActive(false);
        gameManager.animScene_end[1].SetActive(true);
    }

    public void end_anim3()
    {
        gameManager.animScene_end[1].SetActive(false);
        gameManager.animScene_end[2].SetActive(true);
    }

    public void end_anim4()
    {
        gameManager.animScene_end[2].SetActive(false);
        gameManager.animScene_end[3].SetActive(true);
    }

    public void end_anim5()
    {
        gameManager.animScene_end[3].SetActive(false);
        gameManager.animScene_end[4].SetActive(true);
    }

    public void end_anim6()
    {
        gameManager.animScene_end[4].SetActive(false);
        gameManager.animScene_end[5].SetActive(true);

        if (gameManager.isTrueEnd)
        {

            AkSoundEngine.PostEvent("OutroAniStop", gameObject);

        }
        
    }
    public void end_anim7()
    {
        if(gameManager.isTrueEnd)
        {
            trueend_anim1();
            AkSoundEngine.PostEvent("TrueEndAni", gameObject);

        }
        else{
            gameManager.animScene_end[6].SetActive(true);            
        }

    }

    public void end_animfinished()
    {
        gameManager.Invoke("EndAnimFinished",1);
        AnimBG.SetActive(false);
    }
    

    public void trueend_anim1()
    {
        skipEndButton.SetActive(true);
        gameManager.animScene_trueEnd[0].SetActive(true);
        
    }

    public void trueend_anim2()
    {
        
        gameManager.animScene_trueEnd[1].SetActive(true);    
        gameManager.animScene_trueEnd[0].SetActive(false);
    }

    public void trueend_anim3()
    {        
        gameManager.animScene_trueEnd[2].SetActive(true);    
        gameManager.animScene_trueEnd[1].SetActive(false);
    }

    public void skipStartAnim()
    {
        AnimBG.SetActive(false);
        gameManager.animScene_start[0].SetActive(false);
        gameManager.animScene_start[1].SetActive(false);
        //AkSoundEngine.StopAll();
        AkSoundEngine.PostEvent("IntroAniStop", gameObject);
     
        if(!donotShowWannaTutorial){
            askTutorial.SetActive(true);            
        }
        else
        {
            gameManager.EnablePlayerControll();
        }
        playerAnim.enabled=true;
        playerAnim.Play("Idle");
    }

    public void skipEndAnim()
    {
        AnimBG.SetActive(false);
        gameManager.animScene_end[0].SetActive(true);
        gameManager.animScene_end[1].SetActive(true);
        gameManager.animScene_end[2].SetActive(true);
        gameManager.animScene_end[3].SetActive(true);
        gameManager.animScene_end[4].SetActive(true);
        gameManager.animScene_end[5].SetActive(true);
        gameManager.animScene_end[6].SetActive(true);
        gameManager.animScene_trueEnd[0].SetActive(false);
        gameManager.animScene_trueEnd[1].SetActive(false);
        gameManager.animScene_trueEnd[2].SetActive(false);

        end_animfinished();
        //AkSoundEngine.StopAll();
        //AkSoundEngine.PostEvent("IntroAniStop", gameObject);
     
        skipEndButton.SetActive(false);
    }

}
