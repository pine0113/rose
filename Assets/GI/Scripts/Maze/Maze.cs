using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MazeGenarate{

    public enum MazeNodeType
    {
        Wall,
        Ground
    }


    public enum MazeNodeGenColor
    {   Grey,
        Yellow,
        Red,
        Blue
    };

    public enum TileColor
    {
        Red,
        Green,
        Blue,
        White

    }
    public enum WallType{
        normal,
        trigger
    }

    public class TileMaze 
    {
        public int width{get;set;}
        public int height{get;set;}
        public TileMazeNode[,] nodes{get;set;}
        public TileMazeNode entryNode{get;set;}
        public TileMazeNode exitNode{get;set;}

        public TileMaze(int width, int height)
        {   
            this.width = width;
            this.height = height;
            

            Maze maze = new Maze(width,height);
            nodes = new TileMazeNode[width,height];

            for(int y=0; y<height; y++)
            {
                for(int x=0;x<width;x++)                
                {
                    nodes[x,y]=new TileMazeNode();
                }
            }

            for(int x=0;x<width;x++)
            {
                for(int y=0; y<height;y++)
                {
                    nodes[x,y] = new TileMazeNode(maze.nodes[x,y]);

                    if(nodes[x,y].IsEntry)
                      entryNode = nodes[x,y];

                    if(nodes[x,y].IsExit)
                       exitNode = nodes[x,y];
                }
                
            }

            for(int y=0; y<height; y++)
            {
                for(int x=0;x<width;x++)                
                {
                    
                    if(x>0)
                    {
                        nodes[x,y].left = nodes[x-1,y];
                        nodes[x,y].neiborhood.Add(nodes[x-1,y]);
                    }

                    if(y>0)
                    {
                        nodes[x,y].down = nodes[x,y-1];
                            nodes[x,y].neiborhood.Add(nodes[x,y-1]);
                    }

                    if(x<width-2)
                    {
                        nodes[x,y].right = nodes[x+1,y];
                        nodes[x,y].neiborhood.Add(nodes[x+1,y]);
                    }

                    if(y<height-2)
                    {
                        nodes[x,y].up = nodes[x,y+1];
                        nodes[x,y].neiborhood.Add(nodes[x,y+1]);
                    }

                }
            }

  
            
            RandomFloor();
            RandomWall();            
            Debug.Log("Finished TileMaze Generation");

        }
        private void RandomFloor()
        {
            foreach(TileMazeNode node in nodes)
            {            
                if(node.Type == MazeNodeType.Ground){
                    if(node.IsEntry || node.IsExit)
                    {
                         node.floorColor = TileColor.White; 
                    }else{
                        int r =  Random.Range(0,3);                            
                        switch(r)
                        {
                            case 0:   node.floorColor = TileColor.Red; break;
                            case 1:   node.floorColor = TileColor.Green;  break; 
                            case 2:   node.floorColor = TileColor.Blue;   break;
                        }     
                    }       
                }
            }

        }

        private void RandomWall()
        {
            foreach(TileMazeNode n in nodes)
            {
                if(n.Type==MazeNodeType.Wall && n.IsEdge == false)
                {   
                    int r =  Random.Range(0,10);
                    switch(r)
                    {
                        case 0:   n.wallType=WallType.trigger;
                            n.triggerCost_R = Random.Range(1,16)*15;
                            n.triggerCost_G = Random.Range(1,16)*15;
                            n.triggerCost_B = Random.Range(1,16)*15;

                        break;
                        default:  n.wallType=WallType.normal; break;
                    }  
                }
            }
        }

    }

    public class TileMazeNode : MazeNode
    {
        public new List<TileMazeNode> neiborhood {get;set;}
        public new TileMazeNode up {get;set;}
        public new TileMazeNode down {get;set;}
        public new TileMazeNode left {get;set;}
        public new TileMazeNode right {get;set;}
        public TileColor floorColor { get; set; }
        public WallType wallType {get;set;}

        public  TileMazeNode()
        {
            neiborhood = new List<TileMazeNode>();
        }
        public TileMazeNode(int value, MazeNodeGenColor color): base (value , color)
        {
            neiborhood = new List<TileMazeNode>();
            this.value = value;
            this.NodeColor = color;
        }

        public TileMazeNode(MazeNode n):base(n)
        {
            neiborhood = new List<TileMazeNode>();
            this.x=n.x;
            this.y=n.y;
            this.value=n.value;
            this.NodeColor = n.NodeColor; 
            this.Type=n.Type;
            this.IsEdge=n.IsEdge;
            this.IsEnd=n.IsEnd;
            this.IsExit=n.IsExit;
            this.IsEntry=n.IsEntry;
        }

    }


    public class Maze 
    {
        public MazeNode[,] nodes{get;set;}
        public List<MazeNode> endNodes{get;set;}
        public MazeNode entryNode{get;set;}
        public MazeNode exitNode{get;set;}

        public int width{get;set;}
        public int height{get;set;}
        public Maze(int width, int height)
        {

            this.width = width;
            this.height = height;   

            List<MazeNode> blueNodes = new List<MazeNode>();
            List<MazeNode> redNodes = new List<MazeNode>();

            List<MazeNode> startNoesPool= new List<MazeNode>();
            
            nodes = new MazeNode[width,height];

            for(int y=0; y<height; y++)
            {
                for(int x=0;x<width;x++)
                {
                    
                    nodes[x,y]=new MazeNode(1,MazeNodeGenColor.Yellow);
                
                    if(y%2==0)
                    {
                        nodes[x,y].value=0;
                        nodes[x,y].NodeColor=MazeNodeGenColor.Grey;  
                    }else
                    {
                        if(x==1 || x== width-2)
                        {
                            startNoesPool.Add(nodes[x,y]);
                        }
                    }

                    if(x%2==0){
                        
                        nodes[x,y].value=0;
                        nodes[x,y].NodeColor=MazeNodeGenColor.Grey;
                    }else
                    {
                        if(y==1 || y== height-2)
                        {
                            startNoesPool.Add(nodes[x,y]);
                        }
                    }

                    if( (x==0) || (y ==0) || (x==width-1) || (y == height-1 ))
                    {
                        nodes[x,y].IsEdge=true;
                    }
                    nodes[x,y].x=x; 
                    nodes[x,y].y=y; 

                }
            }

            for(int y=0; y<height; y++)
            {
                for(int x=0;x<width;x++)                
                {
                    if(x>0)
                    {
                        nodes[x,y].left = nodes[x-1,y];
                        nodes[x,y].neiborhood.Add(nodes[x-1,y]);
                    }

                    if(y>0)
                    {
                        nodes[x,y].down = nodes[x,y-1];
                            nodes[x,y].neiborhood.Add(nodes[x,y-1]);
                    }

                    if(x<width-2)
                    {
                        nodes[x,y].right = nodes[x+1,y];
                        nodes[x,y].neiborhood.Add(nodes[x+1,y]);
                    }

                    if(y<height-2)
                    {
                        nodes[x,y].up = nodes[x,y+1];
                        nodes[x,y].neiborhood.Add(nodes[x,y+1]);
                    }

                }
            }
            
            int index = Random.Range(0,startNoesPool.Count);
            startNoesPool[index].NodeColor=MazeNodeGenColor.Red;
            redNodes.Add(startNoesPool[index]);
            
            int counter = 0;
            do{
                ProcessPoint(redNodes,blueNodes);
                counter ++;

            }while( blueNodes.Count>0 );           

            
            foreach(MazeNode n in redNodes)
            {
                n.Type=MazeNodeType.Ground;                    
            }

            foreach(MazeNode n in nodes)
            {
                if(n.NodeColor==MazeNodeGenColor.Grey)
                    {
                       n.Type=MazeNodeType.Wall;
                    }
            }

            EndNodesCollect(redNodes);

            entryNode = findMazeEntry();    
            exitNode = findMazeExit(width+height-6,entryNode);        

        }

        public void ProcessPoint(List<MazeNode> redNodes, List<MazeNode> blueNodes)
        {
            var random = new System.Random();   
            int redIndex = random.Next(redNodes.Count);
            //Debug.Log("redNoes.Count:"+redNodes.Count+",redIndex:"+redIndex);

            //paint blue
            paintBlue(redNodes[redIndex],blueNodes);
             
            //select random blue node
            if (blueNodes.Count == 0)
                return;
                
            random = new System.Random(); 
            int blueIndex = random.Next(blueNodes.Count);
            //Debug.Log("bluNodes.Count:"+blueNodes.Count+",blueIndexs:"+blueIndex);
            MazeNode bNode = blueNodes[blueIndex];
            MazeNode nNode = null;

            bool yellow_exist=false;


            if(bNode.up != null && bNode.down != null){
                if(bNode.up.NodeColor==MazeNodeGenColor.Red && bNode.down.NodeColor==MazeNodeGenColor.Yellow && bNode.down.value ==1)
                {                    
                    nNode=bNode.down;   
                    yellow_exist = true;                                                                            
                }
                
                if(bNode.down.NodeColor==MazeNodeGenColor.Red && bNode.up.NodeColor==MazeNodeGenColor.Yellow && bNode.up.value ==1 )
                {                    
                    nNode=bNode.up;
                    yellow_exist = true;  
                        
                }                 
            }
            if(bNode.right != null && bNode.left != null){
                if(bNode.left.NodeColor==MazeNodeGenColor.Red && bNode.right.NodeColor==MazeNodeGenColor.Yellow && bNode.right.value ==1)
                    {
                    nNode=bNode.right;
                    yellow_exist = true;  
                    }


                if(bNode.right.NodeColor==MazeNodeGenColor.Red && bNode.left.NodeColor==MazeNodeGenColor.Yellow && bNode.left.value ==1)
                {                    
                    nNode=bNode.left;
                    yellow_exist = true;                      
                }
            }

            if(yellow_exist){
                nNode.NodeColor=MazeNodeGenColor.Red; 
                redNodes.Add(nNode);
                blueNodes.Remove(nNode); 

                bNode.NodeColor=MazeNodeGenColor.Red;
                redNodes.Add(bNode);
                paintBlue(nNode,blueNodes); 
            }else{
                bNode.NodeColor=MazeNodeGenColor.Grey;  
            }                                                           
                        
            blueNodes.Remove(bNode);       

        }   

        private MazeNode findMazeEntry()
        {      
            MazeNode node;
            node = randomCorner();
            node.IsEntry = true;
            return node;

        }

        private void EndNodesCollect(List<MazeNode> redNodes)
        {
            endNodes=new List<MazeNode>();
            foreach(MazeNode n in redNodes)
            {
                int direct = 4;
                foreach(MazeNode neibor in n.neiborhood)
                {
                    if (neibor != null)
                        if (neibor.NodeColor==MazeNodeGenColor.Red)
                            direct --;
                }
                
                if (direct == 3)
                {
                    n.IsEnd = true;
                    endNodes.Add(n);
                }

            }
        }

        private MazeNode randomEndNode()
        {

            int r =  Random.Range(0,endNodes.Count);
            return endNodes[r];
        }

        private MazeNode randomCorner()
        {
            MazeNode node = new MazeNode(0,MazeNodeGenColor.Grey);
            int r =  Random.Range(0,4);
            switch(r)
            {
                case 0:
                    node = nodes[1,1]; break;
                case 1:
                    node = nodes[1,height-2]; break;
                case 2:
                    node = nodes[width-2,1]; break;
                case 3:
                    node = nodes[width-2,height-2]; break;
            }
            return node;
        }

        private MazeNode findMazeExit(int RequirePathLength, MazeNode entryNode)
        {
            if (entryNode == null)
                return null;

            List<MazeNode> candidate = new List<MazeNode>();
            Dictionary<MazeNode,int> pathCount = new Dictionary<MazeNode,int>();    

            CountPath(pathCount,1, entryNode);
            
            foreach( var pair  in pathCount)
            {
                if( pair.Value >= RequirePathLength && pair.Key.IsEnd == true)
                {
                    candidate.Add(pair.Key);
                }
            }

            int r  = Random.Range(0,candidate.Count);

            candidate[r].IsExit = true;
            
            return candidate[r];
        }

        private void CountPath(Dictionary<MazeNode,int> pathCount, int count, MazeNode now)
        {
            foreach(MazeNode n in now.neiborhood)
            {
                if (n!=null && !pathCount.ContainsKey(n) ) 
                {
                    if(n.NodeColor==MazeNodeGenColor.Red)
                    {   pathCount.Add(n,count); 
                        CountPath(pathCount,count+1,n);
                    }
                }
            }
        }
        
        public void paintBlue(MazeNode node, List<MazeNode> blueNodes)
        {
            foreach (MazeNode neibor in node.neiborhood)
            {
                if(neibor.NodeColor==MazeNodeGenColor.Grey && neibor.value==0 && neibor.IsEdge==false)
                {
                    neibor.NodeColor=MazeNodeGenColor.Blue;
                    blueNodes.Add(neibor);
                }
            }
        }

    }


    public class MazeNode 
    {
        public MazeNode(){}
        public MazeNode(MazeNode n)
        {
            this.value = n.value;
            this.NodeColor = n.NodeColor;
        }
        public MazeNode(int value, MazeNodeGenColor color)
        {
            this.value = value;
            this.NodeColor = color;
            this.neiborhood = new List<MazeNode>();
        }
        public int value {get;set;}
        public MazeNodeGenColor NodeColor {get;set;}
        public MazeNodeType Type{get;set;}
        public int x{get;set;}
        public int y{get;set;}
        
        public bool IsEdge{get;set;}
        public bool IsEntry{get;set;}
        public bool IsExit{get;set;}
        public bool IsEnd{get;set;}

        public MazeNode up {get;set;}
        public MazeNode down {get;set;}
        public MazeNode left {get;set;}
        public MazeNode right {get;set;}

        public List<MazeNode> neiborhood{get;set;}
        public int triggerCost_R;
        public int triggerCost_G;
        public int triggerCost_B;

    }


    
}

