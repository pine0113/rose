using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using MazeGenarate;
public class MazeController : MonoBehaviour
{

    [SerializeField]
    GameObject Floor_W;
    [SerializeField]
    GameObject Floor_R;
    [SerializeField]
    GameObject Floor_G;
    [SerializeField]
    GameObject Floor_B;
    [SerializeField]
    GameObject Floor_Entry;
    [SerializeField]
    GameObject Floor_Exit;
    [SerializeField]
    GameObject Wall;
    [SerializeField]
    GameObject Trigger;

    [SerializeField]
    GameObject FloorContainer;
    private TileMaze maze;
    
    [SerializeField]
    private Tilemap groundTilemap;

    [SerializeField]
    private Tilemap wallTilemap;

    [SerializeField]
    private Tile YellowTile;

    [SerializeField]
    private Tile WhiteTile;

    [SerializeField]
    private Tile RedTile;
    [SerializeField]
    private Tile GreyTile;
    [SerializeField]
    private Tile BlueTile;
    [SerializeField]
    private Tile GreenTile;
  [SerializeField]
    private Tile TriggerTile;
    private bool isMapChange;

    
    private GameObject[,] redTiles;
    private GameObject[,] greenTiles;
    private GameObject[,] blueTiles;
    private GameObject[,] floorTiles;
    private GameObject[,] wallTiles;
    private GameObject[,] triggerTiles;

    

    // Start is called before the first frame update
    void Awake()
    {
        //isMapChange=false; 
        //DrawMazeOnTileMap();
    }

    // Update is called once per frame
    void Update()
    {
        if(isMapChange)
        {
            DrawMazeOnTileMap();
            isMapChange=false;
        }   
    }

    public void GenerateMaze(int width, int height)
    {
       maze=new TileMaze(width,height);
       initBoard(width,height);
       isMapChange=true;
    }


    private void DrawMazeOnTileMap()
    {
            // foreach (Transform child in FloorContainer.transform) {
            //  GameObject.Destroy(child.gameObject);
            // }
        if(maze == null)
            return;

        if (maze.height <= 3 || maze.width <= 3)
            return;

            for (int y = maze.height - 1; y >= 0; y--)
            {
                for (int x = 0; x < maze.width; x++)
                {
                    DrawMazeNode(x, y);
                }
            }
    }

    private void initBoard(int x, int y)
    {
        redTiles = new GameObject[x,y];
        greenTiles = new GameObject[x,y];
        blueTiles = new GameObject[x,y];
        wallTiles = new GameObject[x,y];
        floorTiles = new GameObject[x,y];
        triggerTiles = new GameObject[x,y];

        for(int i=0;i<x;i++)
        {
            for(int j=0;j<y;j++)
            {
                redTiles[i,j]=Instantiate(Floor_R, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                greenTiles[i,j]=Instantiate(Floor_G, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                blueTiles[i,j]=Instantiate(Floor_B, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                floorTiles[i,j]=Instantiate(Floor_W, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                wallTiles[i,j]=Instantiate(Wall, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                triggerTiles[i,j]=Instantiate(Trigger, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
            }

        }


    }
    public void DrawMazeNode(int x, int y)
    {
        redTiles[x,y].SetActive(false);
        greenTiles[x,y].SetActive(false);
        blueTiles[x,y].SetActive(false);
        floorTiles[x,y].SetActive(false);
        wallTiles[x,y].SetActive(false);
        triggerTiles[x,y].SetActive(false);
     
        if (maze.nodes[x, y].Type == MazeNodeType.Ground)
            {
                if ( !maze.nodes[x,y].IsEntry  && !maze.nodes[x,y].IsExit){
                    wallTilemap.SetTile(new Vector3Int(x,y,0), null);
                    switch (maze.nodes[x, y].floorColor)
                    {
                        case TileColor.Red: groundTilemap.SetTile(new Vector3Int(x, y, 0), RedTile); 
                            redTiles[x,y].SetActive(true);
                        
                            // Instantiate(Floor_R, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                        break;
                        case TileColor.Green: groundTilemap.SetTile(new Vector3Int(x, y, 0), GreenTile); 
                            greenTiles[x,y].SetActive(true);                            
                            // Instantiate(Floor_G, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                        break;
                        case TileColor.Blue: groundTilemap.SetTile(new Vector3Int(x, y, 0), BlueTile); 
                            blueTiles[x,y].SetActive(true);
                            // Instantiate(Floor_B, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                        break;
                        case TileColor.White: groundTilemap.SetTile(new Vector3Int(x, y, 0), WhiteTile); 
                            floorTiles[x,y].SetActive(true);
                         //Instantiate(Floor_W, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                        break;
                    }
                }

            }
            else
            {   
                    switch (maze.nodes[x, y].wallType)
                    {
                        case WallType.normal:
                            wallTilemap.SetTile(new Vector3Int(x, y, 0), GreyTile);
                            //Instantiate(Wall, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                            wallTiles[x,y].SetActive(true);
                             break;
                        case WallType.trigger:
                            wallTilemap.SetTile(new Vector3Int(x, y, 0), TriggerTile); 
                            //Instantiate(Trigger, new Vector3(i+0.5f, j+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                            triggerTiles[x,y].SetActive(true);
                            break;
                            
                    }
                
            }


            if (maze.nodes[x,y].IsEntry || maze.nodes[x,y].IsExit)
            {
                    groundTilemap.SetTile(new Vector3Int(x, y, 0), WhiteTile);
                    floorTiles[x,y].SetActive(false);
            }

            if (maze.nodes[x,y].IsEntry)
            {
                Instantiate(Floor_Entry, new Vector3(x+0.5f, y+0.5f, 2), Quaternion.identity,FloorContainer.transform);
                
            }

            if (maze.nodes[x,y].IsExit)
            {
                Instantiate(Floor_Exit, new Vector3(x+0.5f, y+0.5f, 2), Quaternion.identity,FloorContainer.transform);
            }
    }
    public void TransferTriggerToWall(Vector2 wolrdPosition)
    {
      Vector3Int gridposition = groundTilemap.WorldToCell(wolrdPosition);
      TransferTriggerToWall(gridposition.x,gridposition.y);
      isMapChange=true;
    }
    public void TransferTriggerToWall(int x, int y)
    {
        maze.nodes[x,y].Type= MazeNodeType.Wall;
        maze.nodes[x,y].wallType=WallType.normal;
        maze.nodes[x,y].floorColor=TileColor.White;

        redTiles[x,y].SetActive(false);
        greenTiles[x,y].SetActive(false);
        blueTiles[x,y].SetActive(false);
        floorTiles[x,y].SetActive(false);
        wallTiles[x,y].SetActive(true);
        triggerTiles[x,y].SetActive(false);
    }

    public void TransferWallToGround(Vector2 wolrdPosition)
    {
      Vector3Int gridposition = groundTilemap.WorldToCell(wolrdPosition);
      TransferWallToGround(gridposition.x,gridposition.y);
      isMapChange=true;
    }
    public void TransferWallToGround(int x, int y)
    {
        maze.nodes[x,y].Type= MazeNodeType.Ground;
        maze.nodes[x,y].wallType=WallType.normal;
        maze.nodes[x,y].floorColor=TileColor.White;

        redTiles[x,y].SetActive(false);
        greenTiles[x,y].SetActive(false);
        blueTiles[x,y].SetActive(false);
        floorTiles[x,y].SetActive(true);
        wallTiles[x,y].SetActive(false);
        triggerTiles[x,y].SetActive(false);
    }

    public int[] GetWorldTriggerCost(Vector2 position)
    {    
        int[] cost = new int[3];
        //Vector3Int gridposition = groundTilemap.WorldToCell(wolrdPosition);
        cost[0]=maze.nodes[(int)position.x,(int)position.y].triggerCost_R;
        cost[1]=maze.nodes[(int)position.x,(int)position.y].triggerCost_G;
        cost[2]=maze.nodes[(int)position.x,(int)position.y].triggerCost_B;
        return cost;
    }

    public TileColor GetWorldTileColor(Vector2 wolrdPosition)
    {
        Vector3Int gridposition = groundTilemap.WorldToCell(wolrdPosition);
        return maze.nodes[gridposition.x,gridposition.y].floorColor;
    }

    public Vector3 GetWorldEntryPosition()
    {
        Vector3 position = groundTilemap.CellToWorld(new Vector3Int(maze.entryNode.x,maze.entryNode.y,0));

        return new Vector3(position.x+0.5f,position.y+0.5f,0);
    }

    public bool IsPositionOnExist(Vector3 position)
    {
        Vector3Int gridposition = groundTilemap.WorldToCell(position);
        if(maze.exitNode.x == gridposition.x && maze.exitNode.y == gridposition.y)
        {
                return true;
        }
        return false;
    }

    public bool IsPositionMovable(Vector3 position)
    {
        Vector3Int gridposition = groundTilemap.WorldToCell(position);
        if (!groundTilemap.HasTile(gridposition) || wallTilemap.HasTile(gridposition))
            return false;

        return true;
    }

    public bool IsPosistionTrigger(Vector3 position)
    {
        Vector3Int gridposition = groundTilemap.WorldToCell(position);
        if(maze.nodes[gridposition.x,gridposition.y].Type ==MazeNodeType.Wall && maze.nodes[gridposition.x,gridposition.y].wallType==WallType.trigger)
        {
                return true;
        }
        return false;

    }
    public bool IsPositionWall(Vector3 position)
    {
       Vector3Int gridposition = groundTilemap.WorldToCell(position);
       if(maze.nodes[gridposition.x,gridposition.y].Type ==MazeNodeType.Wall && maze.nodes[gridposition.x,gridposition.y].wallType==WallType.normal)
        {
                return true;
        }
        return false;

    }

}
