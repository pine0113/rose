using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerControll : MonoBehaviour
{

    public List<Flower> flowerList;
    public Dictionary<string, int> Answer;
    
    
    // Start is called before the first frame update
    void Start()
    {
        loadFlowerList();
        loadFlowerAnswer();
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int saveFlowerRecord(int R, int G, int B)
    { 
        Debug.Log(R.ToString()+","+G.ToString()+","+B.ToString()+":"+checkAnswer(R,G,B));
        unlockFlower(checkAnswer(R,G,B));
        return checkAnswer(R,G,B);

    }
    public int checkAnswer(int R, int G, int B)
    {
        return Answer[R.ToString()+","+G.ToString()+","+B.ToString()];
    }

    public void loadFlowerList()
    {
        var flowerData = Resources.Load<TextAsset>("flowerList");   
        string[] data =  flowerData.text.Replace("\r","").Split(new char[] {'\n'});
        flowerList = new  List<Flower>();
        
        for(int i =0 ;i < data.Length;i++)
        {
            string[] row = data[i].Split(new char[] {','});
            Flower f = new Flower(i,row[0],row[1],row[2]);
            flowerList.Add(f);
        }      

        
        for(int i =0 ;i < data.Length;i++)
        {
            int status = PlayerPrefs.GetInt("Flower"+i.ToString()+"isUnlock");

            if (status == 1)
            {          
            flowerList[i].isUnlock = true;
            }

            
        }  
      
    }

    private void loadFlowerAnswer()
    {

      var flowerAnswer = Resources.Load<TextAsset>("flowerAnswer");
      var data =  flowerAnswer.text.Replace("\r","").Split(new char[] {'\n'});
      
      Answer = new Dictionary<string, int>();
      for(int i =0 ;i < data.Length;i++)
      {
        string[] row = data[i].Split('\"');
        Answer.Add(row[1],int.Parse(row[2].Replace(",","")));

      }   
    }

    public void unlockFlower(int no)
    {
      flowerList[no].isUnlock = true;
      PlayerPrefs.SetInt("Flower"+no.ToString()+"isUnlock",1);        
    }


    public void lockFlower(int no)
    {
      flowerList[no].isUnlock = false;
      PlayerPrefs.SetInt("Flower"+no.ToString()+"isUnlock",0);        
    }

}
