using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flower 
{
    int FlowerNo=0;
    public string FlowerName ="";
    public string FlowerHint ="";
    public string AbilityText ="";

    public bool isUnlock=false;
    public Flower(int flowerNo, string flowerName, string flowerHint, string abilityText)
    {
        FlowerNo = flowerNo;
        FlowerName = flowerName;
        FlowerHint = flowerHint;
        AbilityText = abilityText;
    }

}