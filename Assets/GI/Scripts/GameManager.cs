using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject BMask;
    public GameObject Effect_R;
    public GameObject Effect_G;
    public GameObject Effect_B;
    public GameObject buffR;
    public GameObject buffG;
    public GameObject buffB;
    public GameObject BuffContainer;


    public GameObject Rplus;
    public GameObject Rminus;
    public GameObject Gplus;
    public GameObject Gminus;
    public GameObject Bplus;
    public GameObject Bminus;

    public FlowerControll flowerControll;
    public SceneControll sceneControll;
    public AnimControll animControll;

    static GameManager instance;
    public GameStatus gameStatus;
    public RGBStatus rGBStatus;
    public PlayerController playerController;
    public MazeController mazeController;
    public CameraController cameraController;
    public UIControllGame uIControllGame;
    public UIControllGeneral uIControllGeneral;
    public Transform playerPosition;
    public Player player;
    public int visionStepCount=0;
    public float BmaskChangeSpeed=2f;
    public float BmaskScale=3f;
    public Vector3 triggerLocation;    
    public int[] triggerCost; 
    public GameObject popup;
    public event Action GetInputHandler;
    public event Action PlayerHitWallHandler;
    public event Action PlayerHitTriggerHandler;
    public event Action PlayerUsingSkillHandler;
    public event Action PlayerArriveMazeEndHandler;
    public event Action PlayerStepOnTileHandler;
    public event Action PlayerRGBValueChangedHandler;
    public const int ValueUnit = 15;
    public string selectedSkill="";
    private Animator m_Animator;
    private bool donotShowWannaTutorial;
    private bool isRevert=false;
    private int flower_no;
    private int skillcost;
    private int skillvalue;
    public GameObject[] animScene_start;
    public GameObject[] animScene_end;
    public GameObject[] animScene_trueEnd;
    public GameObject[] LeafList;
    public bool isTrueEnd=false;

    private void Awake() {
        //PlayerPrefs.DeleteAll();
        if(instance != null)
        {
            Destroy(gameObject);
        }
        instance = this;       
        
        gameStatus = new GameStatus();
        rGBStatus = new RGBStatus();
        m_Animator = player.GetComponent<Animator>();

        animScene_start=new GameObject[2];
        animScene_end=new GameObject[7];
        animScene_trueEnd= new GameObject[3];

        animScene_start[0]=GameObject.Find("start anim scene1");
        animScene_start[1]=GameObject.Find("start anim scene2");

        animScene_end[0]=GameObject.Find("end anim screne1");
        animScene_end[1]=GameObject.Find("end anim screne2");
        animScene_end[2]=GameObject.Find("end anim screne3");
        animScene_end[3]=GameObject.Find("end anim screne4");
        animScene_end[4]=GameObject.Find("end anim screne5");
        animScene_end[5]=GameObject.Find("end anim screne6");
        animScene_end[6]=GameObject.Find("end anim screne7");

        animScene_trueEnd[0]=GameObject.Find("True end screne1");
        animScene_trueEnd[1]=GameObject.Find("True end screne2");
        animScene_trueEnd[2]=GameObject.Find("True end screne3");
        
        LeafList=GameObject.FindGameObjectsWithTag("leaf");

         foreach (GameObject obj in animScene_start){
             obj.SetActive(false);
         }

         foreach (GameObject obj in animScene_end){
             obj.SetActive(false);
         }

         foreach (GameObject obj in animScene_trueEnd){
             obj.SetActive(false);
         }
    }
    

    // Start is called before the first frame update
    void Start()
    {
        //if (Screen.width != 1920) {
        //Screen.SetResolution(1920, 1280, false);
        // set to true if you want a fullscreen game
        //}   
        donotShowWannaTutorial= Convert.ToBoolean(PlayerPrefs.GetInt("DonotShowWannaTutorial"));
        //GameObject.Find("Toggle").GetComponent<Toggle>().isOn = donotShowWannaTutorial;

    }


    void UpdateAnimLeafColor(){

        foreach(GameObject leaf in LeafList)
        {
            leaf.GetComponent<Image> ().color=rGBStatus.color;
        }        

    }
    // Update is called once per frame
    void Update()
    {
        switch(gameStatus)
        {
            case GameStatus.Prepare:
                InitialEventTrigger();
                InitialGameParameter();
                InitialUISettings();
                UpdateAnimLeafColor();            
                PlayerController._isEnable = false;
                animControll.start_anim1();                
                gameStatus=GameStatus.Running;
                
            break;

            case GameStatus.Running:  

                    var moveVector = Vector2.zero;
                    
                    if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
                    {
                        moveVector = Vector2.up;
                    }
                    else if (Input.GetKey(KeyCode.S)|| Input.GetKey(KeyCode.DownArrow))
                    {
                        moveVector = Vector2.down;
                    }

                    if (Input.GetKey(KeyCode.A)|| Input.GetKey(KeyCode.LeftArrow))
                    {
                        moveVector = Vector2.left;
                    }
                    else if (Input.GetKey(KeyCode.D)|| Input.GetKey(KeyCode.RightArrow))
                    {
                        moveVector = Vector2.right;
                    }

                    if(playerController.isEnable())
                    {
                        if(moveVector!=Vector2.zero)
                        { 
                            playerController.Disable();

                
                            if(IsBlockWalkable(moveVector))
                            {

                                playerController.PlayerMove(moveVector);  
                                                   
                                foreach (Transform child in BuffContainer.transform) {
                                    GameObject.Destroy(child.gameObject);
                                }       
                            }
                            else
                            {
                                if(IsBlockTrigger(moveVector))
                                {            
                                    player.UpdateSprite(moveVector);
                                        triggerLocation = playerController.GetPlayerPosition();
                                        triggerLocation.x += moveVector.x;
                                        triggerLocation.y += moveVector.y;

                                        triggerCost = mazeController.GetWorldTriggerCost(triggerLocation);
                                        Debug.Log(triggerCost[0]+","+triggerCost[1]+","+triggerCost[2]);
                                       // triggerCost_B = cost[2];
                                       // triggerCost_R = cost[0];
                                       // triggerCost_G = cost[1];

                                    uIControllGame.EnableSkillPanel();
                                } else{
                                    if(IsBlockWall(moveVector))
                                    {
                                        playerController.PlayerBlocked(moveVector);
                                    }
                                }
                            }                       
                        }
                    }

            break;

            case GameStatus.CalculateScore:               
            break;
        }                        


    }

    void FixedUpdate() 
    {
        icon_R();    
        icon_G();
        BMask.transform.position = playerController.GetPlayerPosition();
        
        if(Rplus!=null){
                if(rGBStatus.colorStatus[ColorType.red]==ColorStatus.add){
                    Rplus.SetActive(true);
                    Rminus.SetActive(false);
                }
                else{
                    Rplus.SetActive(false);
                    Rminus.SetActive(true);
                }

                if(rGBStatus.colorStatus[ColorType.green]==ColorStatus.add){
                Gplus.SetActive(true);
                Gminus.SetActive(false);
                }
                else{
                Gplus.SetActive(false);
                Gminus.SetActive(true);
                }

                if(rGBStatus.colorStatus[ColorType.blue]==ColorStatus.add){
                Bplus.SetActive(true);
                Bminus.SetActive(false);

                }
                else{
                Bplus.SetActive(false);
                Bminus.SetActive(true);
                }
        }
    }

    public void EnablePlayerControll()
    {
        playerController.Enable();
    }

    public void DisablePlayerControll()
    {
        playerController.Disable();
    }

    #region Event
    private void InitialEventTrigger()
    {
        player.MoveFinishedHandler += CheckIsStepOnExist;
        
        PlayerArriveMazeEndHandler += CalculateScore;

        PlayerStepOnTileHandler += OnTileEffect;
        PlayerUsingSkillHandler += AfterSkill;
    }
    #endregion

    #region UI controll
    private void InitialUISettings()
    {
        uIControllGame.updatePlayerRGBStatus(rGBStatus.colorValue[ColorType.red],rGBStatus.colorValue[ColorType.green],rGBStatus.colorValue[ColorType.blue]);
    }
    #endregion

    #region Game Logic

    private bool IsBlockWalkable(Vector2 movement)
    {
        Vector3 position = playerController.GetPlayerPosition();
        position.x += movement.x;
        position.y += movement.y;
        
        return mazeController.IsPositionMovable(position);
    }

    private bool IsBlockWall(Vector2 movement)
    {
        Vector3 position = playerController.GetPlayerPosition();
        position.x += movement.x;
        position.y += movement.y;
        
        return mazeController.IsPositionWall(position);
    }

    private bool IsBlockTrigger(Vector2 movement)
    {
        Vector3 position = playerController.GetPlayerPosition();
        position.x += movement.x;
        position.y += movement.y;
        
        return mazeController.IsPosistionTrigger(position);
    }

    private void InitialGameParameter()
    {
      
        
        player.GetComponent<Animator>().enabled = false;
        mazeController.GenerateMaze(17,17); 
        rGBStatus = new RGBStatus();
        updatePlayerColor();

        //put charactor on start point
        Vector3 entryLocation = mazeController.GetWorldEntryPosition();
        playerController.SetPlayerPosition(entryLocation);
        //cameraController.SetPosition(entryLocation);
    }

    private void CheckIsStepOnExist()
    {
        if(IsPositionOnExist(playerController.GetPlayerPosition()))
        {
            PlayerArriveMazeEndHandler.Invoke();
        }
        else
        {
            PlayerStepOnTileHandler.Invoke();
        }
        
    }

    private bool IsPositionOnExist(Vector3 position)
    {   
        return mazeController.IsPositionOnExist(position);

    }
    private void OnTileEffect()
    {
        //考慮Player Buff


        //新增、修改 RGB 值

        MazeGenarate.TileColor c = mazeController.GetWorldTileColor(playerController.GetPlayerPosition());                         

        /* 顯示特效 */
        switch(c)
        {
            //case MazeGenarate.TileColor.Red: Instantiate(popup,playerController.GetPlayerPosition(), Quaternion.identity); break;//ColorValuePopup.Create(ColorType.red,rGBStatus.colorStatus[ColorType.red],15,player.transform.position); break;
            //case FloorColor.Green: ColorValuePopup.Create(ColorType.green,rGBStatus.colorStatus[ColorType.green],15,player.transform.position); break;
            //case FloorColor.Blue: ColorValuePopup.Create(ColorType.blue,rGBStatus.colorStatus[ColorType.blue],15,player.transform.position); break;
        }

        // add status Value
        switch(c)
        {
            case MazeGenarate.TileColor.Red: rGBStatus.ChangeColorValue(ColorType.red,ValueUnit); break;
            case MazeGenarate.TileColor.Green: rGBStatus.ChangeColorValue(ColorType.green,ValueUnit); break;
            case MazeGenarate.TileColor.Blue: rGBStatus.ChangeColorValue(ColorType.blue,ValueUnit); break;
        }

        //update User顏色
        updatePlayerColor();

        //update UI RGB value
        updatePlayerRGBStatus();

        //RGB值修改特效
        mazeController.TransferWallToGround(playerController.GetPlayerPosition());
        visionStepCount-=1; 
    }

    private void CalculateScore()
    {
      
        
        gameStatus = GameStatus.CalculateScore;
        flower_no = flowerControll.saveFlowerRecord(rGBStatus.colorValue[ColorType.red],rGBStatus.colorValue[ColorType.green],rGBStatus.colorValue[ColorType.blue]);

        if(flower_no==7)
        {
            isTrueEnd = true;
        }
        PlayerPrefs.SetInt("CollectPage",flower_no);      
        UpdateAnimLeafColor();
        animControll.end_anim1();        

    }

    private void EndAnimFinished()
    {
        sceneControll.GoToCollectionScene(flower_no);
    }

    #endregion

    private void updatePlayerRGBStatus()
    {
       
        uIControllGame.updatePlayerRGBStatus( rGBStatus.colorValue[ColorType.red], rGBStatus.colorValue[ColorType.green], rGBStatus.colorValue[ColorType.blue]);
    }
    private void updatePlayerColor()
    {
        player.transform.Find("leaf").GetComponent<SpriteRenderer>().color= (rGBStatus.color);
    }

    private void AfterSkill()
    {
       
        uIControllGame.updatePlayerRGBStatus(rGBStatus.colorValue[ColorType.red],rGBStatus.colorValue[ColorType.green],rGBStatus.colorValue[ColorType.blue]);
        uIControllGame.DisableSkillPanel();
        uIControllGame.EnableMenuButton();
        
    }

    public void confirmSkill(string color)
    {
        selectedSkill = color;
        uIControllGeneral.ReloadSkillConfirmPanel(color,triggerCost);
        uIControllGeneral.EnableSkillConfirmPanel();
        
        switch(color)
        {
            case "red":  skillvalue = rGBStatus.colorValue[ColorType.red];skillcost = triggerCost[0]; break;
            case "green": skillvalue = rGBStatus.colorValue[ColorType.green]; skillcost = triggerCost[1]; break;
            case "blue": skillvalue = rGBStatus.colorValue[ColorType.blue]; skillcost = triggerCost[2]; break;
        }
       
    }

    public void usingSkill()
    {
        uIControllGeneral.DisableSkillConfirmPanel();
        if(skillvalue>skillcost){        
            switch(selectedSkill)
            {
                case "red": skill_R(); break;
                case "green": skill_G(); break;
                case "blue": skill_B(); break;
            }        
        }else
        {        
            uIControllGame.EnableYouCantUse();
        }
    }

    public void skill_R()
    {
        rGBStatus.colorValue[ColorType.red] -= triggerCost[0];        
        Instantiate(Effect_R, playerController.GetPlayerPosition(), Quaternion.identity, BuffContainer.transform);
        mazeController.TransferWallToGround(triggerLocation);
        PlayerUsingSkillHandler.Invoke();
        
        StartCoroutine(icon_R());
        AkSoundEngine.PostEvent("Gear_Effect03", gameObject);
    }

    public void skill_G()
    {

        visionStepCount=5; 
        rGBStatus.colorValue[ColorType.green] -= triggerCost[1];
        
        mazeController.TransferTriggerToWall(triggerLocation);
        Instantiate(Effect_G, playerController.GetPlayerPosition(), Quaternion.identity, BuffContainer.transform);
        PlayerUsingSkillHandler.Invoke();        
        

        AkSoundEngine.PostEvent("Gear_Effect02", gameObject);
    }

    public void skill_B()
    {
        rGBStatus.colorValue[ColorType.blue] -= triggerCost[2];
        rGBStatus.colorStatus[ColorType.red]= rGBStatus.colorStatus[ColorType.red]==ColorStatus.add?ColorStatus.minus:ColorStatus.add;
        rGBStatus.colorStatus[ColorType.blue]= rGBStatus.colorStatus[ColorType.blue]==ColorStatus.add?ColorStatus.minus:ColorStatus.add;
        rGBStatus.colorStatus[ColorType.green]= rGBStatus.colorStatus[ColorType.green]==ColorStatus.add?ColorStatus.minus:ColorStatus.add;
        
        Instantiate(Effect_B, playerController.GetPlayerPosition(), Quaternion.identity, BuffContainer.transform);
        isRevert=!isRevert;
        icon_B();
        mazeController.TransferTriggerToWall(triggerLocation);
        PlayerUsingSkillHandler.Invoke();
        AkSoundEngine.PostEvent("Gear_Effect01", gameObject);
    }

     public void trueend_test()
        {  
            rGBStatus.colorValue[ColorType.blue]=255;
            rGBStatus.colorValue[ColorType.green]=0;
            rGBStatus.colorValue[ColorType.red]=0;
            CalculateScore();
        }
        
    public void changeTutorial()
    {        
        PlayerPrefs.SetInt("DonotShowWannaTutorial", Convert.ToInt32(GameObject.Find("Toggle").GetComponent<Toggle>().isOn));
    }
    IEnumerator icon_R(){

       buffR.GetComponent<Image>().color=new Color(255,0,0,255);
       yield return new WaitForSeconds(1);
       buffR.GetComponent<Image>().color=new Color(0f,0f,0f,45f/255f);
    }
    
    private void icon_G()
    {

         if(visionStepCount>0)
        {    buffG.GetComponent<Image>().color=new Color(0,255,0,255);
             if(BmaskScale < 6.0f)
             {
                BmaskScale = BmaskScale+Time.deltaTime*BmaskChangeSpeed;
                
             }else
             {
                BmaskScale = 6.0f;
             }
        }else
        {
            if(BmaskScale > 3.0f)
            {
               BmaskScale = BmaskScale-Time.deltaTime*BmaskChangeSpeed;
            }
            else
            {
                BmaskScale = 3.0f;
            }
             buffG.GetComponent<Image>().color=new Color(0f,0f,0f,45f/255f);           
        }

        BMask.transform.localScale=new Vector3(BmaskScale,BmaskScale,BmaskScale);
             
    }
    private void icon_B()
    {

        if(isRevert)
        {
            buffB.GetComponent<Image>().color=new Color(0,0,255,255);

        }else
        {
            buffB.GetComponent<Image>().color=new Color(0f,0f,0f,45f/255f);
        }   
       

    }

    public enum GameStatus
    {
        Prepare,
        Running,
        CalculateScore
    }

    public enum ColorType
    {
        red,
        green,
        blue
    }
    public enum ColorStatus
    {
        add,
        minus

    }

    public class RGBStatus
    {
        public Dictionary<ColorType,int> colorValue;
        public Dictionary<ColorType,ColorStatus> colorStatus;
        public Color color;

        public RGBStatus()
        {
            int r = UnityEngine.Random.Range(0,18)*15;
            int g = UnityEngine.Random.Range(0,18)*15;
            int b = UnityEngine.Random.Range(0,18)*15;
            colorValue= new Dictionary<ColorType, int>();
            colorValue.Add(ColorType.red,r);
            colorValue.Add(ColorType.green,g);
            colorValue.Add(ColorType.blue,b);

            colorStatus=new Dictionary<ColorType, ColorStatus>();
            colorStatus.Add(ColorType.red,ColorStatus.add);
            colorStatus.Add(ColorType.green,ColorStatus.add);
            colorStatus.Add(ColorType.blue,ColorStatus.add);
            updateColor();    
        }

        public void ChangeColorValue(ColorType colorType, int value)
        {
            if(colorStatus[colorType]==ColorStatus.add)
            {
                colorValue[colorType] =  (colorValue[colorType]+value >= 255)? 255 : colorValue[colorType]+value;    

                if(colorValue[colorType]==255)
                {
                    colorStatus[colorType]=ColorStatus.minus;
                }
                
                
            }else
            {
                colorValue[colorType] =  (colorValue[colorType]-value <= 0)? 0 : colorValue[colorType]-value;

                if(colorValue[colorType]==0)
                {   colorStatus[colorType]=ColorStatus.add;                
                }

            }

            updateColor();
        }
        
        public void updateColor()
        {
            color = new Color((float)colorValue[ColorType.red]/255,(float)colorValue[ColorType.green]/255,(float)colorValue[ColorType.blue]/255);
        }

       

    }

}
