/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID CHARATER_STEP = 892923668U;
        static const AkUniqueID CHARATER_STEP_WALL = 407024811U;
        static const AkUniqueID COLLECT_BUTTON = 1902243368U;
        static const AkUniqueID EXIT_BUTTON = 477596604U;
        static const AkUniqueID GAMEPLAY_MUSIC = 2322231365U;
        static const AkUniqueID GEAR_CLICK = 1251175901U;
        static const AkUniqueID GEAR_EFFECT01 = 660025893U;
        static const AkUniqueID GEAR_EFFECT02 = 660025894U;
        static const AkUniqueID GEAR_EFFECT03 = 660025895U;
        static const AkUniqueID GEAR_OPEN = 3653461371U;
        static const AkUniqueID INTROANI = 2875468025U;
        static const AkUniqueID INTROANISTOP = 2159427841U;
        static const AkUniqueID INTROMUSIC = 2539580210U;
        static const AkUniqueID INTROMUSICSTOP = 453460554U;
        static const AkUniqueID OUTROANI = 1910271996U;
        static const AkUniqueID OUTROANISTOP = 2859496148U;
        static const AkUniqueID SETTING = 2358904919U;
        static const AkUniqueID SKIPBUTTON = 759057024U;
        static const AkUniqueID START_BUTTON01 = 2881843131U;
        static const AkUniqueID START_BUTTON02 = 2881843128U;
        static const AkUniqueID TRUEENDANI = 381295214U;
        static const AkUniqueID TRUEENDANISTOP = 1408240102U;
        static const AkUniqueID TURN_PAGE = 715928126U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID MUSICVOLUME = 2346531308U;
        static const AkUniqueID PLAYBACK_RATE = 1524500807U;
        static const AkUniqueID RPM = 796049864U;
        static const AkUniqueID SFXVOLUME = 988953028U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID COLLECT = 2756333705U;
        static const AkUniqueID ENTRY = 2184290995U;
        static const AkUniqueID GAMEPLAY = 89505537U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SFX = 393239870U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
